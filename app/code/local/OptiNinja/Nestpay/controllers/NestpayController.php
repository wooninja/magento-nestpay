<?php
class OptiNinja_Nestpay_NestpayController extends Mage_Core_Controller_Front_Action
{
	/**
	 * URL, kamor je obiskovalec preusmerjen, ko je naročilo oddano (zapisano v bazo).
	 */

	public function redirectAction() {
		$this->loadLayout();
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template','Nestpay',array('template' => 'nestpay/redirect.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout(); 
	}

	public function successAction() {

		$settings = Mage::getStoreConfig('payment/nestpay');

		$orderNo = $_POST["ReturnOid"];

		if ( $_POST["clientid"] != $settings["merchant_id"] ) {
			return;
		}

		if ( !isset( $_POST["HASHPARAMS"] ) ) {
			return;
		}

		// Preberemo narocilo iz baze.
		if ( !isset( $_POST["ReturnOid"] ) )
		{
			throw new Exception("Ne najdem naročila!");
		}

		$order = Mage::getModel('sales/order')->loadByIncrementId($orderNo);

		if ( $_POST["Response"] == "Approved" ) {
			//$ordera->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true)->save(); 

			$comment = Mage::helper('nestpay')->__('Transaction successfull');

	        $order->setData('state', $settings['order_status_paid'] );
	        $order->setStatus( $settings['order_status_paid'] );       
	        $history = $order->addStatusHistoryComment($comment, false);
	        $history->setIsCustomerNotified(false);
	        $order->save();




			$oid = $_POST["ReturnOid"];
			
			$request= "DATA=<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?>
			<CC5Request>
			<Name>{NAME}</Name>
			<Password>{PASSWORD}</Password>
			<ClientId>{CLIENTID}</ClientId>
			<OrderId>{OID}</OrderId>	
			<Mode>P</Mode>
			<Extra><ORDERHISTORY>QUERY</ORDERHISTORY></Extra>
			</CC5Request>";
			
			$request=str_replace("{NAME}",$settings["apiusername"],$request);
			$request=str_replace("{PASSWORD}",$settings["apipass"],$request);
			$request=str_replace("{CLIENTID}",$settings["merchant_id"],$request);
			$request=str_replace("{OID}",$oid,$request);

			
			
			$ch = curl_init();								
			curl_setopt($ch, CURLOPT_URL,$settings["apiurl"]);				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);		
			curl_setopt($ch, CURLOPT_TIMEOUT, 95);			
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			
			$result = curl_exec($ch);

			
			if (curl_errno($ch)) 
			{
				//print curl_error($ch);
				//return;
			} 
			else 
			{
				curl_close($ch);
			}


			$url = Mage::getUrl('checkout/onepage/success');
			Mage::app()->getResponse()->setRedirect( $url );

		} else {

			$url = Mage::getUrl('nestpay/nestpay/error', array('_secure'=>true));
			Mage::app()->getResponse()->setRedirect( $url );
		}

	}

	public function errorAction() {

		$settings = Mage::getStoreConfig('payment/nestpay');


		$data = array(
			"error" => Mage::helper('nestpay')->__('Transaction failed'),
		);

		if ( isset( $_POST["mdErrorMsg"] ) ) {
			$data["error"] = $_POST["mdErrorMsg"];
		}

		if ( !isset( $_POST["ReturnOid"] ) )
		{
			$data["error"] = "Naročilo ni bilo najdeno!";
		}

		$orderNo = $_POST["ReturnOid"];

		if ( $_POST["clientid"] != $settings["merchant_id"] ) {
			$data["error"] = $_POST["mdErrorMsg"];
		}

		if ( !isset( $_POST["HASHPARAMS"] ) ) {
			$data["error"] = $_POST["mdErrorMsg"];
		}

		if ( isset( $_POST["mdErrorMsg"] ) ) {
			$data["error"] = $_POST["mdErrorMsg"];
		}


		$order = Mage::getModel('sales/order')->loadByIncrementId($orderNo);

			//$ordera->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true)->save(); 

		$comment = Mage::helper('nestpay')->__('Transaction failed');

        $order->setData('state', $settings['order_status_failed'] );
        $order->setStatus( $settings['order_status_failed'] );       
        $history = $order->addStatusHistoryComment($comment, false);
        $history->setIsCustomerNotified(false);
        $order->save();

        Mage::register('data', $data);


		$this->loadLayout();
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template','Nestpay',array('template' => 'nestpay/failed.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout(); 

	
	}

}
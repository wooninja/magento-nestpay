<?php
class OptiNinja_Nestpay_Model_Nestpay extends Mage_Payment_Model_Method_Abstract
{
	protected $_code = 'nestpay';
	protected $_isGateway = true;
	protected $_canAuthorize = true;

	protected function _getCheckout()  {
		return Mage::getSingleton('checkout/session'); 
	} 

	public function getOrderPlaceRedirectUrl()  {  
		$url = Mage::getUrl('nestpay/nestpay/redirect');

		/// Temporary solution
		$url = str_replace("Nestpay", "nestpay", $url);
		return $url;
	}

}
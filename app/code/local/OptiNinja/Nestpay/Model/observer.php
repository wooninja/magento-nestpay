<?php

class OptiNinja_Nestpay_Model_Observer extends Varien_Event_Observer{
	
	private function _sendOrderEmail($orderID)
	{
		$order = Mage::getModel("sales/order")->load($orderID); //load order by order id

		switch ($order->getPayment()->getMethodInstance()->getCode())
		{
			// -----------------------------------------------------------------
			case 'ispcpay':
			case 'ispcdiners':
			// -----------------------------------------------------------------

				$settings = Mage::getStoreConfig('payment/ispcpay');

				// Pridobi ID transakcije
				$payment_id = Elitek_Custompayment_Model_Ispcpay_Narocilo::getPaymentIdByOrderId($orderID);

				$narocilo = Mage::getModel('custompayment/ispcpay_narocilo');
				$narocilo->getData($payment_id);

				if ($narocilo->isSuccessful())
				{
					$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, sprintf(Mage::helper('custompayment')->__('Payment successfull (transaction ID: %d, amount: %.2f %s)'), $payment_id, $narocilo->getAmount(), $narocilo->getCurrencyCode()));
					$order->save();

					if ($settings['sendemailtocustomer'] == 1)
					{
						$this->sendMailToCustomer($order);
					}

					if ($settings['sendemailtoshop'] == 1 && $settings['shopemail'] != '')
					{
						$this->sendMailToShop($order, $settings['shopemail']);
					}

					Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
				}
				else
				{
					$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW, Mage::helper('custompayment')->__('Payment failed!'));
					$order->save();

					Mage::getSingleton('core/session')->addError('Transakcija je bila zavrnjena s strani plačilnega sistema - plačilo ni bilo izvedeno!');

					Mage::app()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'))->sendResponse();
					exit;
				}
				
				break;

			// -----------------------------------------------------------------
			case 'bankartpay':
                        case 'bankartpayrepay':
			// -----------------------------------------------------------------

				$settings = Mage::getStoreConfig('payment/bankartpay');

				// Pridobi ID transakcije
				$ba_order = Elitek_Custompayment_Model_Bankartpay_Order::fetchByOrderId($order->getIncrementId());

				if ( ! $ba_order)
				{
					Mage::log("Ne najdem naročila za MG naročilo #$orderID", Zend_Log::ERR, 'bankart.log');

					return;
				}

				// Posodobi status naročila
				if ($ba_order->isSuccessful())
				{
					$comment = sprintf(Mage::helper('custompayment')->__('Payment successfull (transaction ID: %d)'), $ba_order->getPaymentID());
					
                                        // Rok dodatek:
                                        if ($order->getPayment()->getMethodInstance()->getCode() === 'bankartpayrepay')
                                        {
                                                $comment .= sprintf(Mage::helper('custompayment')->__(', Number of installments: %d'), $ba_order->getudf1());
                                        }
                                        
					if (isset($settings['order_status_paid']) AND $settings['order_status_paid'])
					{
						$order->addStatusToHistory($settings['order_status_paid'], $comment);
					}
					else
					{
						$order->addStatusHistoryComment($comment);
					}

					if ($settings['sendemailtocustomer'] == 1)
					{
						$this->sendMailToCustomer($order);
					}
					if ($settings['sendemailtoshop'] == 1 && $settings['shopemail'] != '')
					{
						$this->sendMailToShop($order, $settings['shopemail']);
					}
				}
				else
				{
					$comment = sprintf(Mage::helper('custompayment')->__('Payment failed (transaction ID: %d) - %s'), $ba_order->getPaymentID(), $ba_order->getResult());

					if (isset($settings['order_status_failed']) AND $settings['order_status_failed'])
					{
						$order->addStatusToHistory($settings['order_status_failed'], $comment);
					}
					else
					{
						$order->addStatusHistoryComment($comment);
					}
				}

				$order->save();

				// ??
				Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
				
				break;

			case 'pdfquotepay':
				$this->pdfQuotePayAction($order);
				break;

			case 'monetapay':
				$this->monetaPayAction($order);
				break;
		}
	}	
	
	private function sendMailToCustomer($order){
		$email = $order->getCustomerEmail();
		$emailTemplate = Mage::getModel('core/email_template');
                $emailTemplate->loadDefault('success_custom_mail_customer_tpl');
                $emailTemplate->setTemplateSubject(Mage::helper('custompayment')->__('Payment successful'));
		$senderName = Mage::getStoreConfig('trans_email/ident_support/name'); 
		$senderMail = Mage::getStoreConfig('trans_email/ident_support/email');
		
		$emailTemplate->setSenderName($senderName);
                $emailTemplate->setSenderEmail($senderMail);
		
		$emailVariable['orderid'] = $order->getIncrementId();
		
		$emailTemplate->send($email, $order->getCustomerName(), $emailVariable);

		// Send standard magento "order confirmation" email
		$order->sendNewOrderEmail();
	}
	
	private function sendMailToShop($order, $email){     
		$emailTemplate = Mage::getModel('core/email_template');
                
                $orderCode = $order->getPayment()->getMethodInstance()->getCode();
                if (($orderCode === 'bankartpayrepay') || ($orderCode === 'ispcdiners')) {
                        $emailTemplate->loadDefault('success_custom_mail_shop_tpl_installments');
                } else {
                        $emailTemplate->loadDefault('success_custom_mail_shop_tpl');
                }
                $emailTemplate->setTemplateSubject(Mage::helper('custompayment')->__('Payment successful'));
		$senderName = Mage::getStoreConfig('trans_email/ident_support/name');
		$senderMail = Mage::getStoreConfig('trans_email/ident_support/email');
		
		$emailTemplate->setSenderName($senderName);
                $emailTemplate->setSenderEmail($senderMail);
		
		$emailVariable['orderid'] = $order->getIncrementId();
		
		$emailTemplate->send($email, $order->getStoreName(), $emailVariable);
	}
	
	private function pdfQuotePayAction($order){
		//shrani narocilo z predracunom v bazo
		$coll = Mage::getModel('custompayment/quotes')
			->setQuote_date($order->getCreatedAt())
			->setOrder_id($order->getEntityId())
			->setUser_id($order->getCustomerId())
			->setStore_id($order->getData('store_id'))
			->setStatus($order->getStatus())
			->setQuote_sent(0)
			->save();
		
		//poslje predracun
		$quoteEmail = Mage::getModel('custompayment/pdfquotespay_pdfquoteemail');
		$quoteEmail->setQuoteID($coll->getData('quote_id'));
		$code = $quoteEmail->sendEmail();
		if($code > 100){	//napaka
			//TODO error	
		}else{	//email poslan, poveca koliko krat je bil poslan predracun
			$coll->setQuote_sent(1)->save();
		}
	}
	
	private function monetaPayAction($order){
		
	}

	public function onepageCheckoutDone($event)
	{
		$data = $event->getData();

		if (isset($data['order_ids']))
		{
			// Old way
			$order_id = current($data['order_ids']);

			$this->_sendOrderEmail($order_id);
		}
		
		$this->_empty_cart();
	}

	private function _empty_cart()
	{
//		$cart = Mage::getSingleton('checkout/cart');
//		$quoteItems = Mage::getSingleton('checkout/session')
//						  ->getQuote()
//						  ->getItemsCollection();
//
//		foreach ($quoteItems as $item)
//		{
//			$cart->removeItem( $item->getId() );
//		}
//		$cart->save();

		Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
	}
}
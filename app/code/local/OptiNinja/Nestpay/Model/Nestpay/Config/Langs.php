<?php
class OptiNinja_Nestpay_Model_Nestpay_Config_Langs {

    public function toOptionArray()
    {

        $options = array(
					"SLO" => "Slovenian",
					"USA" => "English",
					"SRB" => "Serbian",
					"ITA" => "Italian",
					"FRA" => "French",
					"DEU" => "German",
					"ESP" => "Espanol",
		);

        return $options;
    }
}

<?php
class OptiNinja_Nestpay_Model_Nestpay_Config_Statuses {

    public function toOptionArray()
    {
        $statuses = Mage::getSingleton('sales/order_config')->getStatuses();

        $options = array();

        foreach ($statuses as $code => $label)
		{
            $options[] = array(
               'value' => $code,
               'label' => $label
            );
        }
		
        return $options;
    }
}

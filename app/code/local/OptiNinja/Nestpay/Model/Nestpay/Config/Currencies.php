<?php
class OptiNinja_Nestpay_Model_Nestpay_Config_Currencies {

    public function toOptionArray()
    {

        $options = array(
					"978" => "EUR",
					"191" => "HRK",
					"941" => "RSD",
					"840" => "USD",
					"643" => "RUB",
					"826" => "GBP",
					"807" => "MKD",
		);

        return $options;
    }
}

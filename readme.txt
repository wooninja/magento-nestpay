=== Nestpay - plačilni sistem za Magento ===
Contributors: mroky
Donate link: http://optininja.com/
Tags: integration, Nestpay, 3dsecure, credit cards, payment gateway
Requires at least: 4.6
Requires PHP: 5.4
Tested up to: 1.9.3.3
Stable tag: 1.9.3.3
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Možnost plačila preko NestPay sistema v Magento spletni trgovini. Razširite razpoložljive plačilne metode in povečajte prodajo.

== Licence ==
Please note, all available code is now open source and no support is provided for installation, use or further development of these extensions. 
You’ll be able to use all the extensions without any time limit. We also give you the freedom to fix bugs, add features, and change the code as you want. 
All extensions are release under GNU General Public License, version 3 (GPL-3.0).

== Changelog ==
= 1.0 =
* Prva verzija